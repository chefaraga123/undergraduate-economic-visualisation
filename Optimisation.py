#Draws the tangency condition for a single agent given their endowment & budget constraint
#And their utility function 

import matplotlib.pyplot as plt

class Agent:
    def __init__(self, endowment, exponents):
        self.endowment = endowment
        self.exponents = exponents

    #the budget constraint for the utility function in t = n describes the agents resources in t = n,
    def Utility_function(self, input1, input2): #Assume a Cobb-douglas utility function 
        part1 = (input1)**(self.exponents[0])
        part2 = (input2)**(self.exponents[1])
        return part1*part2

    def MRS(self, input1, input2):
        MUX1 = self.exponents[0] * (input1 ** (self.exponents[0]-1))
        print(MUX1)
        MUX2 = self.exponents[1] * (input1 ** (self.exponents[1]-1))
        print(MUX2)
        return -(MUX1/ MUX2)

    def budget_constraint(self, economy):
        budget = economy.prices[0] * self.endowment[0] + economy.prices[1] * self.endowment[1]
        maximum_x1 = budget / economy.prices[0]
        
        amount_x1_schedule = []
        amount_x2_schedule = []
        
        for amount_x1 in range(0, int(maximum_x1)):
            amount_x2 = (budget - amount_x1)/economy.prices[1]
            amount_x1_schedule.append(amount_x1)
            amount_x2_schedule.append(amount_x2)

        return [amount_x1_schedule, amount_x2_schedule]

    def tangency_point(self, economy): #assume convex preferences
        for val1 in range(1,100):
            for val2 in range(1,100):
                print('val1:',val1, 'val2:',val2)
                print(self.MRS(val1, val2))
                print(economy.price_ratio())
                if self.MRS(val1, val2) == economy.price_ratio():
                    print(val1, val2)
                    return [val1, val2]

        #return [val1_schedule, val2_schedule]

    def indifference_curves(self):
        amount_x1_schedule = []
        amount_x2_schedule = []
        for val1 in range(0, self.endowment[0]):
            for val2 in range(0, self.endowment[1]):
                if round(Utility_function(self, val1, val2)) == 9:
                    amount_x1_schedule.append(val1)
                    amount_x2_schedule.append(val2)
        return [amount_x1_schedule, amount_x2_schedule] 

class Economy:
    def __init__(self, prices):
        self.prices = prices

    def price_ratio(self): 
        return self.prices[0]/self.prices[1]
    #prices measure the rate at which people are willing to substitute one good for another



def plot_graph():
    plt.figure(figsize=(16,6))

    endowment = [50, 50] 
    exponents = [0.75, 0.25]
    prices = [1, 2]

    agent = Agent(endowment, exponents)
    economy = Economy(prices)
    budget_line = agent.budget_constraint(economy)
    #tangency_point = agent.tangency_point(economy)
    schedules = agent.indifference_curves()

    plt.xlim(0,30)
    plt.ylim(0,30) #define the dimensions of the axis 
    plt.title("Indifference curve & budget line")
    plt.xlabel('Amount of good 1')
    plt.ylabel('Amount of good 2')

    plt.scatter(schedules[0], schedules[1], label="Utility = 4")
    a = budget_line[0]
    b = budget_line[1]
    plt.plot(a, b, label="Budget Line")
    plt.legend()
    plt.show()

plot_graph()