import matplotlib.pyplot as plt
import numpy as np
import math





def draw_canvas():
    plt.figure(figsize=(20,8))
    plt.xlabel("Signal")
    plt.ylabel("Income")
    plt.ylim(0,110)
    plt.xlim(0,50)
    plt.title("Indifference Curves")

def plot_incomes(wages):
    H = wages['yH']
    L = wages['yL']
    plt.axhline(y=H,color='black',linestyle='dashed')
    plt.axhline(y=L,color='black',linestyle='dashed')
    pooling_wage = (H+L)/2
    plt.axhline(y=pooling_wage, label="Pooling Wage", color="red",linestyle='dashed')
#draw employee income in state-income space, income is modelled as an indiffernce curve

def plot_signalling_thresholds(wages):
    s0 = wages['s0'] 
    s1 = wages['s1']
    plt.annotate("s0", (s0,0))
    plt.vlines(x=s0, ymin=0, ymax = wages['yH'],color='black',linestyle='dashed')
    plt.annotate("s1", (s1,0))
    plt.vlines(x=s1, ymin=0, ymax = wages['yH'],color='black',linestyle='dashed')
    # if s0 <= S* <= s1, then the wage schedule is a fully revealing, seperating ICE
    # However the above wage schedule is inefficient if S*>s0


#curves with shallower gradients have lower signalling costs 
def plot_utilities(wages):
    mH = 40/30#wages['cH']
    mL = 40/15#wages['cL']

    s = np.linspace(0,50)#independent variable is amount of signal
    constant_utility = 60
    yH = mH*s + constant_utility
    yL = mL*s + constant_utility

    plt.plot(s, yH, label="High Productivity Worker")
    plt.plot(s, yL, label="Low Productivity Worker")




wages = {
    'yH':100,
    'yL':60,
    'cH':1, #cost per unit of signal
    'cL':1.5,
    's0':15,#the signal boundaries are dynamic
    's1':30
}



draw_canvas()
plot_incomes(wages)
plot_utilities(wages)
plot_signalling_thresholds(wages)

plt.legend()
plt.savefig("Diagrams/symmetric_signalling")
plt.show()