#drawing consumer demand curves

import matplotlib.pyplot as plt
import numpy as np
import math

def draw_canvas():
    plt.figure(figsize=(16,6))
    plt.title("Intertemporal Consumption")
    plt.xlabel("Period 1 Consumption")
    plt.ylabel("Period 2 Consumption")
    plt.xlim(0,30)
    plt.ylim(0,30)
'''
x = np.arange(10)
y = np.arange(10)

plt.xticks(x, " ")
plt.yticks(y, " ")

plt.plot(x, y)

'''

def utility_function(parameters, x1, x2):
    a = parameters['a']
    b = parameters['b']
    return (x1**a)*(x2**b)

def MRS(parameters, x1 ,x2):
    a = parameters['a']
    b = parameters['b']
    return (a*x2)/(b*x1)

def budget_lines(price_ratio, budget):

    w1 = budget['x1']#endowment of x1
    w2 = budget['x2']#endowment of x2

    x2_schedule = []
    x1_schedule = []
    for x in range(0,100):
        x2_schedule.append(w2 + (w1-x)*price_ratio)
        #income + income transferred from other state 
        x1_schedule.append(x)
    plt.plot(x1_schedule,x2_schedule, color="red")

    #lets assume the income is the same in each period 
    plt.hlines(y=w2, xmin=0, xmax=w1,color='black',linestyle='dashed')
    plt.vlines(x=w1, ymin=0, ymax=w2,color='black',linestyle='dashed')
    plt.annotate("Net Saver", (5, 15))
    plt.annotate("Netural", (10, 10))
    plt.annotate("Net Borrower", (15, 4))
    
    return [x1_schedule,x2_schedule]

def indifference_curve(parameters, price_ratio, budget):
    a = parameters['a']
    b = parameters['b']
    w1 = budget['x1']#endowment of x1
    w2 = budget['x2']#endowment of x2
    p2 = price_ratio**(-1)

    for x1 in range(1,int((p2*w2+w1))):
        x2 = price_ratio*(w1-x1)+w2 #the budget constraint
        if round(MRS(parameters, x1, x2),1) == round(price_ratio,1):
            utility = utility_function(parameters, x1, x2)
            plt.hlines(y=x2, xmin=0, xmax=x1,color='black',linestyle='dashed')
            plt.vlines(x=x1, ymin=0, ymax=x2,color='black',linestyle='dashed')
            print('x1:',x1,'x2:',x2)

    x2_schedule = []
    x1_schedule = []

    for x1 in range(1,100):
        c = utility*(x1**(-a))
        d = c**(b**(-1))
        x2_schedule.append(d)
        x1_schedule.append(x1)

    plt.plot(x1_schedule,x2_schedule, label="Utility = {}".format(utility), linewidth=0.5)
    return utility

parameters = {
    'a':0.5,
    'b':0.5
}

budget = {
    'x1':10,
    'x2':10,
}

draw_canvas()

r = 0.2 #interest rate = 10%
price_ratio1 = 1+r 
#consumption c saved in period 1 can be transformed to c(1+r) in period 2 

budget_points = budget_lines(price_ratio1,budget)
#utility_level = indifference_curve(parameters, price_ratio1, budget)



plt.legend()
plt.savefig("Diagrams/IntertemporalConsumption")
plt.show()