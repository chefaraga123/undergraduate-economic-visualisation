import random
import math
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import pandas as pd 
import numpy as np 

#global variables 

class IS_Curve:

    def __init__(self,stabilising_interest_rate,alpha,equilibrium_output):
        self.stabilising_interest_rate = stabilising_interest_rate
        self.alpha = alpha
        self.equilibrium_output = equilibrium_output

    def primary_IS_Relationship(self,previous_period_real_interest_rate):
        monetary_policy_decision = previous_period_real_interest_rate - self.stabilising_interest_rate
        output = self.equilibrium_output - self.alpha*(monetary_policy_decision) 
        return output 
    
    def Secondary_IS_Relationship(self,new_output):
        output_gap = self.equilibrium_output - new_output
        interest_rate =  output_gap/self.alpha + self.stabilising_interest_rate
        return interest_rate
    
    def IS_Schedule(self):
        output_schedule = []
        interest_rates = []
        for previous_period_real_interest_rate in range(-5,15):
            actual_output = self.primary_IS_Relationship(previous_period_real_interest_rate)
            interest_rates.append(previous_period_real_interest_rate)
            output_schedule.append(actual_output)
        return [output_schedule,interest_rates]

#self,stabilising_interest_rate,alpha,equilibrium_output
IS_curve_1 = IS_Curve(3, 1, 100)
IS_curve_2 = IS_Curve(3, 1, 90) #significant negative demand shock
IS_curve_3 = IS_Curve(3, 1, 98) #impact of announcement over medium-term rates
IS_curve_4 = IS_Curve(3, 1, 105) #impact of announcement over medium-term rates

IS_Schedule_1 = IS_curve_1.IS_Schedule()
IS_Schedule_2 = IS_curve_2.IS_Schedule()
IS_Schedule_3 = IS_curve_3.IS_Schedule()
IS_Schedule_4 = IS_curve_4.IS_Schedule()


plt.figure(figsize=(16,6))
plt.xlim(90,110)
plt.ylim(-5,10)
plt.plot(IS_Schedule_1[0] ,IS_Schedule_1[1], label="Pre-Shock")
plt.plot(IS_Schedule_2[0] ,IS_Schedule_2[1], label="Post-Shock")
plt.plot(IS_Schedule_3[0] ,IS_Schedule_3[1], label="Post-Announcement")
#plt.plot(IS_Schedule_4[0] ,IS_Schedule_4[1], label="Committment To Irresponsibility")


stabilising_interest_rate = 2
equilibrium_output = 100

#draw inflation target and equilibrium output 
#plt.axhline(y=stabilising_interest_rate,color='black',linestyle='dashed', label="Stabilising Interest Rate")
plt.axhline(y=0,color='black')

plt.axvline(x=equilibrium_output,color='black',linestyle='dashed')

plt.legend()

plt.xlabel("Output")
plt.ylabel("Interest Rate (%)")
plt.savefig("Diagrams/Week3Diagram")


plt.show()