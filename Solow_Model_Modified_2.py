'''
- this is the modified version 
- The solow model augmented to consider the impact of population growth 
- How do changes in the population growth rate affect the steady state 
  level of capital per worker?
- output per worker (Y/L)
- output per unit effective labour (Y/AL)
- Thus variables shall be reconsidered in per capita terms
'''


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math
import random 


#A = technology, K = capital, E = labour, e = exponent, n = population growth rate


'''
 - Define the relationships between variables 
 - capital & labour's share of incomes 
 - Depreciation & capital investment 
'''
def per_capita_production_function(capital_per_worker,exponent): #assume Cobb-Douglass
    return capital_per_worker**exponent

def break_even_investment(capital_per_worker, pop_growth, depreciation,technology_growth):
    return (pop_growth + depreciation+ technology_growth)*capital_per_worker



def motion_of_capital(capital_investment, break_even_investment):
    '''
    Capital per worker = Gross capital investment - depreciated capital - increased 
    capital requirements due to population growth 
    need to consider the capital increase required to hold capital per worker constant
    '''
    return capital_investment-break_even_investment

#############################################################################
'''
 - plot the above relationships 
 - output per capita on y-axis
 - capital per worker on the x-axis 
'''


def plot_production_function(exponent):
    production_per_worker = []
    for capital_per_worker in range(0,101):
        production_per_worker.append(per_capita_production_function(capital_per_worker,exponent))
    return production_per_worker

def plot_capital_investment(savings_rate,exponent):
    investments = []
    for val in plot_production_function(exponent):
        investments.append(round(savings_rate*val,2))
    return investments

def plot_capital_depreciation(pop_growth, depreciation,technology_growth):
    capital_depreciation_line = []
    for capital_per_worker in range(0,101):
        capital_depreciation_line.append(round(break_even_investment(capital_per_worker,
        pop_growth, depreciation, technology_growth),1)) #rate% depreciation 
    return capital_depreciation_line

        

plt.figure(figsize=(16, 6))
plt.xlabel("Capital Stock Per effective Worker")
plt.ylabel("Economic Output Per effective Worker")
plt.title("Per Capita Solow Model")

plt.xlim(0, 2)
plt.ylim(0, 2)

savings_rate = 0.02
capital_share_of_income = 0.3
population_growth_rate = 0.01 #1%
depreciation_rate = 0.01

technology_growth = 0.01 #1%
technology_growth_new = 0.02 #2%

total_production = plot_production_function(capital_share_of_income)
investments = plot_capital_investment(savings_rate,capital_share_of_income)
capital_depreciation_line = plot_capital_depreciation(population_growth_rate,depreciation_rate,technology_growth)
capital_depreciation_line_1 = plot_capital_depreciation(population_growth_rate,depreciation_rate,technology_growth_new)




plt.plot(capital_depreciation_line,label='Capital Depreciation')
plt.plot(capital_depreciation_line_1,label='Capital Depreciation')


plt.plot(total_production,label='Exponent = 0.3')
plt.plot(investments, label='Investment Curve')


plt.legend()
plt.savefig("Diagrams/Solow_Model_modified_2.png")
plt.show()


#show point at which no net change in capital 
def show_intersection(a,b, total_production, colour):
    for output in a:
        for val in b:
            if output == val and a.index(output) == b.index(output):
                capital = a.index(output)
                plt.hlines(y=output, xmin=0, xmax=capital, color='black', linestyle='dashed')
                plt.vlines(x=capital, ymin=0, ymax=output, color='black', linestyle='dashed')
                plt.scatter(capital,output, color=colour,s=200)
                #level of capital is the index, now find the level of production 
                #for the given amount of capital 
                c = total_production[capital]
                plt.hlines(y=c, xmin=0, xmax=capital, color='black', linestyle='dashed')
                plt.vlines(x=capital, ymin=0, ymax=c, color='black', linestyle='dashed')
                plt.scatter(capital,c, color=colour,s=200)
                break
