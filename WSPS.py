import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math 
import random

#WS-PS Curve

def labour_supply_curve(gradient):
    wage_schedule = [] 
    for labour_amount in range(0,101):
        wage = gradient * labour_amount 
        wage_schedule.append(wage)
    return wage_schedule
    
#Wage setting curve is a markup on the labour supply curve 
def wage_setting_curve(gradient, bargaining_power):#not necessarily a parralel shift of the labour supply curve 
    wage_schedule = labour_supply_curve(gradient) 
    wage_setting_schedule = [] 
    for wage_val in wage_schedule:
        wage_setting_schedule.append(wage_val * bargaining_power)
    return wage_setting_schedule

#defined by the marginal product of labour - demand increases if worker productivity increases
def labour_demand_curve(labour_productivity, shock_term): 
    wage_schedule = []
    y_intercept = 100
    for labour_amount in range(0,101):
        wage = -(labour_amount * labour_productivity) + y_intercept +shock_term
        wage_schedule.append(wage)
    return wage_schedule

#below the labour demand curve, real wage is below MPL due to firms' market power 
def price_setting_curve(labour_productivity, market_power, shock_term):
    wage_schedule = labour_demand_curve(labour_productivity, shock_term)
    price_setting_schedule = []
    for wage in wage_schedule:
        price_setting_schedule.append(wage-market_power)
    return price_setting_schedule 



class P_Curve:    

    def __init__(self,equilibrium_output,alpha,last_period_inflation):
        self.equilibrium_output = equilibrium_output
        self.alpha = alpha 
        self.last_period_inflation = last_period_inflation

    def P_Relationship(self,actual_output):
        output_gap = actual_output - self.equilibrium_output
        inflation = self.alpha*output_gap + self.last_period_inflation
        return inflation

    def PC_Schedule(self):
        inf_schedule = [] 
        actual_outputs = []
        for actual_output in range(0,100):
            inf = self.P_Relationship(actual_output)#+shift
            inf_schedule.append(inf)
            actual_outputs.append(actual_output)
        return [actual_outputs,inf_schedule]



fig = plt.figure(figsize=(16,16))
ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)

'''
pc1 = P_Curve(32, 1, 2)
pc2 = P_Curve(32, 1.5, 2)
a = pc1.PC_Schedule()
b = pc2.PC_Schedule()
ax2.plot(a[0], a[1], label="alpha = 1"),
ax2.plot(b[0], b[1], label="alpha = 1.5")
ax2.legend()
ax2.set_xlabel("Output")
ax2.set_ylabel("Inflation (%)")
ax2.legend()
'''

gradient = 1 
bargaining_power = 1.5
market_power = 20
labour_productivity = 1
shock_term = 5





ax1.set_xlabel("Employment Level")
ax1.set_ylabel("Wage")
ax1.set_xlim(0,100)
ax1.set_ylim(0,100)


b = wage_setting_curve(gradient, bargaining_power)
ax1.plot(b,label='WS-Curve')
#remains constant

d = price_setting_curve(labour_productivity, market_power,0)
ax1.plot(d, label='PS-Curve')#-1')
#supply shock 
#e = price_setting_curve(labour_productivity, market_power,20)
#ax1.plot(e, label='PS-Curve-2')


ax1.legend()


#plots lines for intersection of WS & PS 
for pos_b, val_b in enumerate(b):
    for pos_d, val_d in enumerate(d):
        if val_b == val_d and pos_b == pos_d:
            ax1.vlines(x = pos_d, ymin=0, ymax = val_d,color='black',linestyle='dashed')
            ax1.hlines(y = val_d, xmin= 0, xmax = pos_d,color='black',linestyle='dashed')
            imperfect_comp_labour_demand = pos_b
            imperfect_comp_wage = val_b
            print(imperfect_comp_wage)

#plot lines given a positive output gap
for pos_b, val_b in enumerate(b):
    for pos_d, val_d in enumerate(d):
        if val_b == val_d and pos_b == pos_d:
            ax1.vlines(x = pos_d+10, ymin=0, ymax = val_d+15,color='black',linestyle='dashed')
            ax1.hlines(y = val_d, xmin= 0, xmax = pos_d+10,color='black',linestyle='dashed')

            ax1.annotate("a", (42,63), fontsize=15)
            ax1.annotate("c", (42,32), fontsize=15)
            ax1.annotate("b", (42,48), fontsize=15)

            imperfect_comp_labour_demand = pos_b
            imperfect_comp_wage = val_b
            print(imperfect_comp_wage)


'''            
for pos_b, val_b in enumerate(b):
    for pos_e, val_e in enumerate(e):
        if val_b == val_e and pos_b == pos_e:
            ax1.vlines(x = pos_e, ymin=0, ymax = val_e ,color='black',linestyle='dashed')
            ax1.hlines(y = val_e, xmin=0, xmax = pos_e ,color='black',linestyle='dashed')
            print(pos_e, val_e)
            imperfect_comp_labour_demand2 = pos_e
            imperfect_comp_wage2 = val_e
''' 

ax2.set_xlabel("Output")
ax2.set_ylabel("Inflation")
ax2.set_xlim(0,100)
ax2.set_ylim(0,10)

print(imperfect_comp_labour_demand)


ax2.vlines(x=imperfect_comp_labour_demand, ymin=0, ymax=90, label="VPC")
ax2.legend()
#'ax2.vlines(x=imperfect_comp_labour_demand2, ymin=0, ymax=90)'
plt.savefig("Diagrams/WSPS")

plt.show()