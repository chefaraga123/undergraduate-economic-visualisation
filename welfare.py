#drawing consumer demand curves

import matplotlib.pyplot as plt
import numpy as np
import math

def draw_canvas():
    plt.figure(figsize=(16,6))
    plt.title("Welfare Changes")
    plt.xlabel("good 1")
    plt.ylabel("good 2")
    plt.xlim(0,60)
    plt.ylim(0,60)

def utility_function(parameters, x1, x2):
    a = parameters['a']
    b = parameters['b']
    return (x1**a)*(x2**b)

def MRS(parameters, x1 ,x2):
    a = parameters['a']
    b = parameters['b']
    return (a*x2)/(b*x1)

def budget_lines(price_ratio, budget):

    w1 = budget['x1']#endowment of x1
    w2 = budget['x2']#endowment of x2

    x2_schedule = []
    x1_schedule = []
    for x in range(0,100):
        x2_schedule.append(w2 + (w1-x)*price_ratio)
        x1_schedule.append(x)

    plt.plot(x1_schedule,x2_schedule, label="p1/p2 = {}".format(price_ratio))
    return [x1_schedule,x2_schedule]

def compensating_variation(utility, price_ratio):#income at new prices to maintain old utility 
    pass

def equivalent_variation():
    pass

def indifference_curve(parameters, price_ratio, budget):
    a = parameters['a']
    b = parameters['b']
    w1 = budget['x1']#endowment of x1
    w2 = budget['x2']#endowment of x2
    p2 = price_ratio**(-1)

    for x1 in range(1,int((p2*w2+w1))):
        x2 = price_ratio*(w1-x1)+w2 #the budget constraint
        if round(MRS(parameters, x1, x2),1) == round(price_ratio,1):
            utility = utility_function(parameters, x1, x2)
            plt.hlines(y=x2, xmin=0, xmax=x1,color='black',linestyle='dashed')
            plt.vlines(x=x1, ymin=0, ymax=x2,color='black',linestyle='dashed')
            print('x1:',x1,'x2:',x2)

    x2_schedule = []
    x1_schedule = []

    for x1 in range(1,100):
        c = utility*(x1**(-a))
        d = c**(b**(-1))
        x2_schedule.append(d)
        x1_schedule.append(x1)

    plt.plot(x1_schedule,x2_schedule, label="Utility = {}".format(utility))
    return utility

parameters = {
    'a':0.5,
    'b':0.5
}

budget = {
    'x1':20,
    'x2':10,
}

draw_canvas()

#this is effectively a function for drawing an indifference curve 
#tangent to a given budget line
price_ratio1 =4/3
budget_points = budget_lines(price_ratio1,budget)
utility_level = indifference_curve(parameters, price_ratio1, budget)

price_ratio2 = 1
budget_points = budget_lines(price_ratio2,budget)
utility_level = indifference_curve(parameters, price_ratio2, budget)


plt.legend()
plt.savefig("Diagrams/Optimisation")
plt.show()