import random
import math
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import pandas as pd 
import numpy as np 


#depreciation of the exchange rate leads to gain in interest rates?

def calculate_exchange_rate(world_interest_rate,expected_depreciaton,interest_rate): #for a given currency depreciation (or appreciation)
    return world_interest_rate + expected_depreciaton - interest_rate 

def UIP_Curve(world_interest_rate):
    UIP_Curve=[]
    for interest_rate in range(0,11):
        UIP_Curve.append(calculate_exchange_rate(world_interest_rate,1,interest_rate))
    return UIP_Curve

def plot_lines(curve1,curve2,curve3, e):
    pass
    #i=i* -> stay same
    #i<i* -> depreciation
    #i>i* -> appreciation
    

UIP_Curve1 = UIP_Curve(1) #world interest rate falls
UIP_Curve2 = UIP_Curve(5) #original 
UIP_Curve3 = UIP_Curve(9) #world interest rate increases

plt.figure(figsize=(16, 6))
plt.xlim(0,12)
plt.ylim(0,12)
plt.xlabel("log Exchange Rate (Depreciation ->)")
plt.ylabel("Interest Rate")
plt.title("Uncovered Interest Parity Curves")

e=3
plot_lines(UIP_Curve1,UIP_Curve2,UIP_Curve3, e)

plt.hlines(y=5, xmin=0, xmax=5, color='black', linestyle='dashed')
plt.vlines(x=5, ymin=0, ymax=5, color='black', linestyle='dashed')
plt.scatter(5,5, s=400, color='red')

plt.hlines(y=5, xmin=0, xmax=1, color='black', linestyle='dashed')
plt.vlines(x=1, ymin=0, ymax=5, color='black', linestyle='dashed')
plt.scatter(1,5, s=400, color='black')

plt.hlines(y=3, xmin=0, xmax=3, color='black', linestyle='dashed')
plt.vlines(x=3, ymin=0, ymax=3, color='black', linestyle='dashed')
plt.scatter(3,3, s=400, color='grey')


plt.plot(UIP_Curve1,label='UIP Curve i<i*')
plt.plot(UIP_Curve2,label='UIP Curve i=i*')
plt.plot(UIP_Curve3,label='UIP Curve i>i*')

plt.legend()

plt.savefig("Diagrams/UIP_Curves.png")
plt.show()