

#Preference rules are functions that assign a different preference ordering 
#for society to different profiles of individual preferences. 

'''
The first two methods take individual voter preference orderings then aggregate them, 
returning a social preference ordering. 
The final method plots voter utilities (the scores each allocation has been assigned)
against the allocations on a single axis. This can show the existence, or absence of 
single-peaked preferences. 
'''


import matplotlib.pyplot as plt

candidates = ['a', 'b', 'c', 'd']

agent_1 = ['a', 'b', 'c', 'd']
agent_2 = ['a', 'b', 'd', 'c']
agent_3 = ['b', 'c', 'd', 'a'] 

candidates_scores = {}
individual_scores = []

for candidate in candidates:
    candidates_scores[candidate] = 0

agent_preferences = [agent_1, agent_2, agent_3]

#given a list of individual preference orderings return scores for each alternative 
def scoring_rule(agent_preferences):
    individual_preferences = []
    for vote_preferences in agent_preferences:#isolates a single agent
        indiviudal_preference = {'a':0,'b':0,'c':0,'d':0}
        for pos, preference in enumerate(vote_preferences):
            indiviudal_preference[preference] = pos+1
            candidates_scores[preference] += (pos+1) 
        individual_preferences.append(indiviudal_preference)
    return [candidates_scores, individual_preferences]

#these scores are thus used to rank the candidates from highest to lowest 
def return_preference_ordering(scores):
    score_values = list(scores.values())
    score_values.sort() #identify the highest score 

    ordering = []
    for val in score_values:
        for val_key in list(scores.items()):
            if val_key[1] == val:
                ordering.append(val_key[0])
    return ordering


aggregated_scores = scoring_rule(agent_preferences)[0]
individuals_scores = scoring_rule(agent_preferences)[1]
ordering = return_preference_ordering(aggregated_scores)

#the above functions can be used 

def plot_individual_preferences(individuals_scores):
    plt.figure(figsize=(16,6))
    plt.xlabel("Candidate")
    plt.ylabel("Candidate Score")
    plt.ylim(1,len(candidates)+0.5)
    candidate_pos = [i for i, _ in enumerate(candidates)]
    plt.xticks(candidate_pos, candidates)
    for voter, individual_score in enumerate(individuals_scores):
        plt.plot(candidates, list(individual_score.values()), label = "Voter {}".format(voter))
    plt.title("Voter Preferences")
    plt.legend()
    plt.savefig("Individual_Preferences")
    plt.show()

plot_individual_preferences(individuals_scores)
