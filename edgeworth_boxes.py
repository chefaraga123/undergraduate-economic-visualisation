import pyEdgeworthBox as eb

%matplotlib inline

EB2=eb.EdgeBox(u1 =lambda x,y: x**2*y, u2=lambda x,y: x+y, IE1=[5,5], IE2=[5,5])
EB2.plot()