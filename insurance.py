import matplotlib.pyplot as plt
import numpy as np
import math

#Insurance
def plot_frame():
    plt.figure(figsize=(10,10))

    plt.title("Two State Model")
    plt.xlabel("Income State 1")
    plt.ylabel("Income State 2")

    plt.xlim(0,110)
    plt.ylim(0,110)
    x = np.arange(0,110,1)
    y = x
    plt.plot(x,y, label="Full Cover Line")

    
def identity_line():
    x = np.arange(0,110,1)
    y = x
    return [x,y]


def utility_function(x):
    return math.log(x)

#for seperating equilibrium
def state_contingent_incomes(contract):

    state_1_schedule = [] #defines the x-axis
    state_2_schedule = [] #defines the y-axis  

    for q in range(100):
        state_1_schedule.append(contract['y'] - contract['P']*q)
        state_2_schedule.append(contract['y'] - contract['L'] + (1-contract['P'])*q)


    return [state_1_schedule, state_2_schedule]

'''
this is essentially a budget constraint line 
where the goods are the level of income in each of the two states
expected utility is high the lower the spread of possible incomes - hence 
where the y1=y2 line intersects the budget line is the expected utility 
maximising point given actuarially fair premiums
actuarially fair premiums are when cost of premium = probability of loss 
The expected income accross the budget line is by definition constant 
given the above. 
''' 

def plot_state_incomes(schedules,label_txt):
    plt.plot(schedules[0],schedules[1], label="{}".format(label_txt))

plot_frame()

contract1 = {
    'P':0.4, #premium  
    'p':0.4, #probability of loss
    'y':100, #income
    'L':50,   #loss
}

contract2 = {
    'P':0.6, #premium  #a higher premium implies a shallower gradient 
    'p':0.6, #probability of loss
    'y':100, #income
    'L':50,   #loss
}

schedules1  = state_contingent_incomes(contract1)
schedules2  = state_contingent_incomes(contract2)

label_txt1 = "Low Premiums" #has lower premium costs
label_txt2 = "High Premiums" # has higher premium costs 
#higher premium costs mean that essentially one can transfer less state-contingent income 
#between states
plot_state_incomes(schedules1,label_txt1)
plot_state_incomes(schedules2,label_txt2)


plt.legend()
plt.show()


