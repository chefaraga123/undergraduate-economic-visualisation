
import matplotlib.pyplot as plt
import numpy as np
import math


#finding the risk premium



def plot_utility_function():
    plt.figure(figsize=(20,8))
    plt.ylim(0, 5)
    plt.xlim(0,40)
    x = np.arange(0.1,40,0.1)
    y = np.log(x)
    plt.plot(x, y, label ="Utility Function", color='black')
    plt.legend()


def utility_function(x):
    return math.log(x)


def risk_premium(lottery):

    p1 = lottery['p']
    p2 = (1-p1)
    win = lottery['win']
    loss = lottery['loss']
    expected_value = p1*win +p2*loss 

    a = p1*utility_function(win)#find the utility of each state
    b = p2*utility_function(loss) 
    expected_utility = a + b #sum of the utilities of each state averaged

    certainty_equivalent = math.e ** expected_utility 
    risk_premium =expected_value - certainty_equivalent 
    print(risk_premium)
    return [risk_premium, expected_utility]


def graph_risk_premium(lottery):
    
    plot_utility_function()
    plt.xlabel("Outcome")
    plt.ylabel("Utility")

    win = lottery['win']
    u_1 = utility_function(win)#y-value for the utility of the win outcome

    loss = lottery['loss']
    u_2 = utility_function(loss)#y-value for the utility of the loss outcome 

    plt.annotate("a", (loss, u_2))
    plt.annotate("b", (win, u_1))

    x = np.linspace(loss, win)
    y = u_2 + ((u_1-u_2)/(win-loss))*(x-loss)
    plt.plot(x,y)

    EV = (win+loss)/2

    x1 = np.linspace(0,win)
    y1 = u_1 + 0*x

    x2 = np.linspace(0,loss)
    y2 = u_2 + 0*x

    #expected utility 
    EU = risk_premium(lottery)[1]
    CE = math.e ** EU 
    x3 = np.linspace(0,EV)
    y3 = EU + 0*x

    plt.vlines(x=CE, ymin=0, ymax=EU, label="Certainty Equivalent", color="black", linestyle='dashed')

    plt.plot(x1,y1, color="black", linestyle='dashed')
    plt.plot(x2,y2, color="black", linestyle='dashed')
    plt.plot(x3,y3, color="black", linestyle='dashed')

    plt.vlines(x=EV, ymin=0, ymax=EU, label="Expected Value", color="black", linestyle='dashed')

    plt.title("Graph to show risk premium")
    plt.savefig("Diagrams/Risk_Premium")

    plt.legend()
    

lottery = {
    'p':0.5,
    'win':20,
    'loss':5,
}



graph_risk_premium(lottery)
plt.show()

