#demand-curve
#supply-curve

import matplotlib.pyplot as plt
import numpy as np
import math

def draw_canvas():
    plt.figure(figsize=(16,6))
    plt.title("Welfare Changes")
    plt.xlabel("Quantity Demanded")
    plt.ylabel("Price Level")
    plt.xlim(1,40)
    plt.ylim(1,40)


def u(x):
    return 30*((x)**(-0.6)) #DRS

def c(x):    
    return 0.1*(x**2) +2 #IRS
        
def U(x):
    return u(x) - c(x)

def draw_benefit_curve():
    benefit_curve = []
    x_schedule = list(range(1,100))
    for x in range(1,100):
        benefit_curve.append(u(x))
    plt.plot(x_schedule, benefit_curve)

def draw_cost_curve():
    cost_curve = []
    x_schedule = list(range(100))
    for x in range(0,100):
        cost_curve.append(c(x))
    plt.plot(x_schedule, cost_curve)

def draw_lines():
    for val in range(1,100):
        if round(U(val)) == 0:
            plt.hlines(y=u(val), xmin=0, xmax=val,color='black',linestyle='dashed')
            plt.vlines(x=val, ymin=0, ymax=u(val),color='black',linestyle='dashed')

            x1 = np.arange(0,val, 0.1)
            y1 = u(x1)
            plt.fill_between(x1,y1,u(val), color="green", label="Consumer Surplus")
            plt.fill_between(x1,u(val),c(x1), color="red", label="Producer Surplus")

draw_canvas()
draw_cost_curve()
draw_benefit_curve()
draw_lines()
plt.legend()
plt.savefig("Diagrams/Consumer_Surplus")
plt.show()