#equilibrium analysis is looking at how the prices adjust so as to make 
# the demand and supply decisions of economic agents compatible 

#the supply curve simply measured how much the consumer was willing to supply the good 
#at each possible market price 

#The Equilibrium price solves:
#   D(P*) = S(P*), P* is the price where market demand = market supply


#Exchange Economy (No Production)
#define two agents with endowments and utility curves

import matplotlib.pyplot as plt 
import numpy as np


class Economy:
    def __init__(self, num_agents, prices):
        self.num_agents = num_agents # how many agents there are in the economy 
        self.prices = prices

    def agent_budgets(self, agents):
        agent_net_values = [] 
        for agent in agents:
            agent_value = 0
            agent_value += agent.budget[0] * self.prices[0] 
            agent_value += agent.budget[1] * self.prices[1]
            agent_net_values.append(agent_value)
        return agent_net_values

    def Exchange(self):
        pass


class Agent:
    def __init__(self, budget, exponents):
        self.budget = budget
        self.exponents = exponents

    #the budget constraint for the utility function in t = n describes the agents resources in t = n,
    def Utility_function(self): #Assume a Cobb-douglas utility function 
        part1 = (self.budget[0])**(self.exponents[0])
        part2 = (self.budget[1])**(self.exponents[1])
        return part1*part2

    def Indifference_curve(self):
        return #to return indifference curves for with utility = budget 

class draw_Edgeworth_box:
    def __init__(self, total_x1, total_x2):
        self.total_x1 = total_x1
        self.total_x2 = total_x2

    def create_box(self): #doesnt return anything just performs action on a figure
        plt.xlim(0, self.total_x1)
        plt.ylim(0, self.total_x2)

    def draw_indifference_curves(self, agents):
        agents[0].Indifference_curve() #return plots for indifference curve at a constant utility
        agents[1].Indifference_curve()

    def draw_budget_constraints(self, economy):
        p1 = economy.prices[0]
        print(p1)
        p2 = economy.prices[1]
        print(p2)
        x1_schedule = []
        x2_schedule = [] 
        
        for val in range(0,self.total_x1+1):
            x1_schedule.append(val)
            x2_schedule.append((-1*(p1/p2)*val) + 4)

        plt.plot(x1_schedule, x2_schedule, label="Budget Line")

def instantiate_agents():
    budget1 = [4, 5]
    exponents1 = [0.6, 0.4]
    agent1 = Agent(budget1, exponents1)

    budget2 = [8, 2]
    exponents2 = [0.5, 0.5]
    agent2 = Agent(budget2, exponents2)

    return [agent1, agent2]


def draw_edgeworth_box(agents):
    prices = [1, 3]# numeraire, p1 = 1, p2 = 3
    economy = Economy(2, prices)

    plt.figure(figsize=(16,6)) #creates a new figure 

    total_x1 = agents[0].budget[0] + agents[1].budget[0]#x-axis
    total_x2 = agents[0].budget[1] + agents[1].budget[1]#y-axis

    edgeworth_box = draw_Edgeworth_box(total_x1, total_x2) 
    edgeworth_box.create_box()#defines the edgeworth box axes/constraints 
    edgeworth_box.draw_budget_constraints(economy)

    plt.legend()
    plt.show()

agents = instantiate_agents()
draw_edgeworth_box(agents)