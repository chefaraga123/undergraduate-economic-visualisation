import matplotlib.pyplot as plt
import numpy as np
import math

def draw_frame():
    plt.figure(figsize=(16,6))
    plt.title("Mergers")
    plt.xlim(0,100)
    plt.ylim(0,100)
    plt.xlabel("Quantity")
    plt.ylabel("Price")


def draw_demand_curve():
    x = np.linspace(0,100)
    y = 80 - x
    plt.plot(x,y)

def draw_costs():

    pre_merger_cost = 80-50
    monopoly_price = 80 - 60

    #price
    plt.hlines(y=60, xmin=0, xmax=monopoly_price, color='black',linestyle='dashed')

    #cost 
    plt.axhline(y=50, color='green') #pre-merger
    plt.axhline(y=20, color='black') #post merger
    
    #demand
    plt.vlines(x=pre_merger_cost, ymin=0, ymax=50, color='green',linestyle='dashed')
    plt.vlines(x=monopoly_price, ymin=0, ymax=60, color='black',linestyle='dashed')

draw_frame()
draw_demand_curve()
draw_costs()
plt.savefig("Monopolising_Merger")
plt.show()