import random
import math
import matplotlib.pyplot as plt

import pandas as pd 
import numpy as np 

class P_Curve:    

    def __init__(self,equilibrium_output,alpha,last_period_inflation):
        self.equilibrium_output = equilibrium_output
        self.alpha = alpha 
        self.last_period_inflation = last_period_inflation

    def P_Relationship(self,actual_output):
        output_gap = actual_output - self.equilibrium_output
        inflation = self.alpha*output_gap + self.last_period_inflation
        return inflation

    def PC_Schedule(self):
        inf_schedule = [] 
        actual_outputs = []
        for actual_output in range(95,110):
            inf = self.P_Relationship(actual_output)#+shift
            inf_schedule.append(inf)
            actual_outputs.append(actual_output)
        return [actual_outputs,inf_schedule]


plt.figure(figsize=(16,6))
pc1 = P_Curve(100,1,2)
pc2 = P_Curve(100, 1.5, 2)
a = pc1.PC_Schedule()
b = pc2.PC_Schedule()
plt.plot(a[0], a[1], label="alpha = 1"),
plt.plot(b[0], b[1], label="alpha = 1.5")
plt.legend()
plt.xlabel("Output")
plt.ylabel("Inflation (%)")
plt.savefig("PhillipsCurves")
plt.show()