import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math
import random 


#A = technology, K = capital, E = labour, e = exponent 


'''
 - Define the relationships between variables 
 - capital & labour's share of incomes 
 - Depreciation & capital investment 
'''
def production_function(productivity,capital,labour,exponent): #assume Cobb-Douglass
    return productivity*(capital**exponent)*(labour**(1-exponent))

def capital_depreciation(capital_stock, depreciation_rate):
    return capital_stock * depreciation_rate

def investment_rate(savings_rate,total_output):
    return total_output * savings_rate 

def capital_accumulation(savings_rate, total_output, capital_stock, depreciation_rate):
    updated_capital_Stock = savings_rate*total_output - capital_depreciation(capital_stock,depreciation_rate)
    return updated_capital_Stock

'''
 - plot the above relationships 
'''

def plot_capital_depreciation(depreciation_rate):
    capital_depreciation_line = []
    for capital_stock in range(0,101):
        capital_depreciation_line.append(round(capital_depreciation(capital_stock,depreciation_rate),1)) #rate% depreciation 
    return capital_depreciation_line

def plot_production_function(exponent):
    total_production = []
    labour = 100
    for capital_stock in range(0,101):
        total_production.append(production_function(1.0,capital_stock,labour,exponent))
    return total_production

def plot_capital_investment(savings_rate):
    investments = []
    for val in plot_production_function(0.33):
        investments.append(round(investment_rate(savings_rate,val),1))
    return investments
        

plt.figure(figsize=(16, 6))
plt.xlabel("Capital Stock")
plt.ylabel("Total Economic Output")
plt.title("Solow Model")

plt.xlim(0, 100)
plt.ylim(0, 100)

capital_depreciation_line = plot_capital_depreciation(0.4)
investments_1 = plot_capital_investment(0.3)

investments_2 = plot_capital_investment(0.35)

#show point at which no net change in capital 
def show_intersection(a,b, total_production, colour):
    #intersection 1
    for output in a:
        for val in b:
            if output == val and a.index(output) == b.index(output):
                capital = a.index(output)
                plt.hlines(y=output, xmin=0, xmax=capital, color='black', linestyle='dashed')
                plt.vlines(x=capital, ymin=0, ymax=output, color='black', linestyle='dashed')
                plt.scatter(capital,output, color=colour,s=200)
                #level of capital is the index, now find the level of production 
                #for the given amount of capital 
                c = total_production[capital]
                plt.hlines(y=c, xmin=0, xmax=capital, color='black', linestyle='dashed')
                plt.vlines(x=capital, ymin=0, ymax=c, color='black', linestyle='dashed')
                plt.scatter(capital,c, color=colour,s=200)
                break

capital_share_of_income_1 = 0.3
total_production_1 = plot_production_function(capital_share_of_income_1)


capital_share_of_income_2 = 0.4
total_production_2 = plot_production_function(capital_share_of_income_2)

#show_intersection(capital_depreciation_line, investments_1, total_production, "red")
#show_intersection(capital_depreciation_line, investments_2, total_production, "green")
#show the increase in steady state total production
#for that i only need the level of capital 



#find where the investment curve intersects the deprectiation schedule 

plt.plot(capital_depreciation_line,label='Capital Depreciation')

plt.plot(total_production_1,label='Exponent = 0.3')
plt.plot(total_production_2,label='Exponent = 0.4')

plt.plot(investments_1, label='Investment Curve')




plt.legend()
plt.savefig("Diagrams/Solow_Model.png")
plt.show()