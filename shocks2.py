# create a diagram showing shocks 
import matplotlib.pyplot as plt
import numpy as np
import math

fig = plt.figure(figsize=(16,16))
ax1 = fig.add_subplot(3, 1, 1)
ax2 = fig.add_subplot(3, 1, 2)
ax3 = fig.add_subplot(3, 1, 3)
ax1.set_xlim(0,5)
ax1.set_ylim(-1,1)

ax2.set_xlim(0,5)
ax2.set_ylim(-5,5)

ax3.set_xlim(0,5)
ax3.set_ylim(-5,5)

time_periods = ['t-1','t','t+1','t+2','t+3' ]
output_gaps = [0,-1,-1,-1,0]

ax1.bar(time_periods, output_gaps)
ax1.text(0,0,"info arrives", fontsize=15)
ax1.axhline(y=0, color ="black")
ax1.set_title("Output Gaps")


inflation_schedule = [] 
for output_gap in output_gaps:
    inflation_schedule.append(sum(inflation_schedule)+output_gap)
ax2.bar(time_periods, inflation_schedule)
ax2.set_title("Inflation given AEPC")


inflation_schedule = [] 
for period, output_gap in enumerate(output_gaps):
    inflation_schedule.append(sum(output_gaps[period:]))
ax3.bar(time_periods, inflation_schedule)
ax3.set_title("Inflation given NKPC")

plt.savefig("Diagrams/info_Arrives_before")

plt.show()